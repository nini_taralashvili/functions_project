//#1 დავალება
//#1.1. შექმენით ფუქნცია (calcAverage), რომელიც დაითვლის 3 ქულის საშუალო არითმეტიკულს
function calcAverage(firstScore,secondScore,thirdScore){
return (firstScore + secondScore + thirdScore)/3;

}

//#1.2 გამოიყენათ ფუნქცია თითოეული გუნდის საშუალოს დასათვლელად
let powerRangers1 = [44, 23, 71];
let fairyTails1 = [65, 54, 49];
let powerRangers2 = [85, 54, 41];
let fairyTails2 = [23, 34, 47];

let firstAverage = calcAverage(...powerRangers1);
let secondAverage = calcAverage(...fairyTails1);
let thirdAverage = calcAverage(...powerRangers2);
let fourthAverage = calcAverage(...fairyTails2);


//#1.3. შექმენათ ფუნქცია რომელიც შეამოწმებს გამარჯვებულ გუნდს (ex. checkWinner),  მიიღებს 
//ორ პარამეტრს გუნდების საშუალოს სახით და კონსოლში დალოგავს გამარჯვებულ გუნდს თავისი ქულებით 
function checkwinner(avrg1,avrg2){
if(avrg1 / avrg2 === 2 ){
    console.log(`Winner is PowerRangers with - ${avrg1} average points`);
}else if(avrg2 / avrg1 === 2){
    console.log(`Winner is FairyTails with - ${avrg2} average points`);
}
else if(avrg1 === avrg2){
    console.log('We Have Draw :))');
}
else{
    console.log("No One Is Winner :((");
}
}
console.log("First Case : ");
checkwinner(firstAverage,secondAverage);
console.log("Second Case : ");
checkwinner(thirdAverage,fourthAverage);

//#2 დავალება
//#2.1. შექმენათ ფუქნცია (calcTip) რომელიც მიიღებს ანგარიშის მნიშნელობას პარამეტრად და დააბრუნებს თიფს 
function calcTip(calculation){
    let tip = 0;
if( calculation <= 50 && calculation <= 300){
tip = 15;
}else{
tip = 20;
}
return tip;
}
//#2.2შექმენით ანგარიშების მასივი
let myArray = [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52];
//#2.3. შექმენათ თიფების მასივი და რომელშიც შეინახავთ ფუნქციის მეშვეობით დათვლის თიფებს.
let tips = [];
for(let element of myArray ){
    tips.push(calcTip(element));
}
console.log("Here Is An Array Of Tips : ");
console.log(tips);
//#2.4. შექმენით მასივი სადაც შეინახავთ ჯამურ თანხას თითოეული ანგარიში + ანგარიშის შესაბამისი თიფი.
let sum = [];
//#2.5 გამოიყენეთ ლუფი კალკულაციების ჩასატარებლად.
for(let i=0; i<myArray.length; i++ ){
    
        sum.push(myArray[i]+tips[i]);
    
}
console.log("Here Is An Array Of Calculations + Tips : ");
console.log(sum);
//#2.6. შექმენათ ფუნქცია რომელიც მიიღებს მასივს პარამეტრად და 
//დააბრუნებს საშუალოს, ამ ფუნქციით უნდა დავითვალოთ საშუალოდ რამდენ ჩაის 
//ტოვებს  ლუდოვიკო (2.3. პუნტში მიღებული მასივი) და 
//ასევე უნდა დავითვალოთ საშუალოდ
 //რა თანხა დახარჯა მანდ ჭამა-სმაში (2.4. პუნქტის მასივის საშუალო არითმეტიკული

 function myAverage(arr){
     let averageArr = 0;
  let mylength = arr.length
for(let i = 0; i < mylength; i++){
    averageArr += arr[i];  
}
console.log(`Here Is An Average -> ${averageArr/mylength}`);
 }


 myAverage(tips);
 myAverage(sum);
